﻿using System;
using RabbitMQ.Client;
using System.Text;
using System.Threading;
using Newtonsoft.Json;

public class Send
{
    public static void Main()
    {
        var factory = new ConnectionFactory()
        {
            HostName = "hawk.rmq.cloudamqp.com",
            VirtualHost = "ftnlvzoc", 
            UserName = "ftnlvzoc", 
            Password = "2PEodjbuL9EcnlNXCP2MNZXVvuBPNtAW"
        };
        using(var connection = factory.CreateConnection())
        using(var channel = connection.CreateModel())
        {
            channel.QueueDeclare(queue: "homework",
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            while (true)
            {
                var ageRandom = new Random();
                var age = ageRandom.Next(0, 100);
                var user = new User.User{Age = age, Name = "Name"};
                
                var trueFalseRandom = new Random();
                var trueFalse = trueFalseRandom.Next(0, 2);
                if (trueFalse >= 1)
                {
                    user.Email = "email@email.ru";
                }
                    
                string message = JsonConvert.SerializeObject(user);
                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                    routingKey: "homework",
                    basicProperties: null,
                    body: body);
                Console.WriteLine(" [x] Sent {0}", message);
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }
        }

        Console.WriteLine(" Press [enter] to exit.");
        Console.ReadLine();
    }
}