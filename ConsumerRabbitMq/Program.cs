﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using Newtonsoft.Json;

class Receive
{
    private static int _unacked = 0;
    private static int _acked = 0;
    public static void Main()
    {
        var factory = new ConnectionFactory()
        {
            HostName = "hawk.rmq.cloudamqp.com",
            VirtualHost = "ftnlvzoc", 
            UserName = "ftnlvzoc", 
            Password = "2PEodjbuL9EcnlNXCP2MNZXVvuBPNtAW"
        };
        using(var connection = factory.CreateConnection())
        using(var channel = connection.CreateModel())
        {
            channel.QueueDeclare(queue: "homework",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                var gettingUser = JsonConvert.DeserializeObject<User.User>(message);
                if (!string.IsNullOrEmpty(gettingUser.Email))
                {
                    _acked++;
                    Console.WriteLine($"Ack {_acked}");
                    channel.BasicAck(ea.DeliveryTag, multiple: false);
                }
                else
                {
                    _unacked++;
                    Console.WriteLine($"No ack {_unacked}");
                }
                Console.WriteLine(" [x] Received {0}", message);
            };
            channel.BasicConsume(queue: "homework",
                autoAck: false,
                consumer: consumer);
            
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
    }
}